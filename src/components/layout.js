import React from 'react';

import Link from './link';
import {rhythm, scale} from '../utils/typography';
import colors from '../utils/colors';

class Layout extends React.Component {
  render() {
    const {location, title, children, js} = this.props;
    const rootPath = `${__PATH_PREFIX__}/`;
    let header;

    if (location.pathname === rootPath) {
      header = (
        <h1
          style={{
            ...scale(1.5),
            marginBottom: rhythm(1.5),
            marginTop: 0,
          }}>
          <Link
            style={{
              boxShadow: `none`,
              textDecoration: `none`,
              color: colors.pink,
            }}
            to={`/`}>
            {title}
          </Link>
        </h1>
      );
    } else {
      header = (
        <h3
          style={{
            fontFamily: `Montserrat, sans-serif`,
            marginTop: 0,
          }}>
          <Link
            style={{
              boxShadow: `none`,
              textDecoration: `none`,
              color: colors.pink,
            }}
            to={`/`}>
            {title}
          </Link>
        </h3>
      );
    }
    return (
      <>
        <div
          style={{
            marginLeft: `auto`,
            marginRight: `auto`,
            maxWidth: rhythm(24),
            padding: `${rhythm(1.5)} ${rhythm(3 / 4)}`,
          }}>
          <header>{header}</header>
          <main>{children}</main>
          <footer>
            <small>this is a footer</small>
          </footer>
        </div>
        {js &&
          js.map(({publicURL}) => (
            <script key={publicURL} src={publicURL} type="text/javascript" />
          ))}
      </>
    );
  }
}

export default Layout;

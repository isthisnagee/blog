import React from "react"
import { Link as GatsbyLink } from "gatsby"
import colors from "../utils/colors"

const Link = props => (
  <GatsbyLink {...props} style={{ ...props.style, color: colors.pink }} />
)

export default Link

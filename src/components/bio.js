/**
 * Bio component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import { useStaticQuery, graphql } from "gatsby"

import { rhythm } from "../utils/typography"

const Bio = () => {
  const data = useStaticQuery(graphql`
    query BioQuery {
      site {
        siteMetadata {
          author
          social {
            twitter
          }
        }
      }
    }
  `)

  const { author, social } = data.site.siteMetadata
  return (
    <div
      style={{
        display: `flex`,
        alignItems: `space-between`,
        marginBottom: rhythm(2.5),
      }}
    >
      <p style={{ marginRight: "10px" }}>
        by <strong>{author}</strong>.
      </p>
      <div style={{ marginRight: "10px" }}>
        <a href={`https://twitter.com/${social.twitter}`}>twitter</a>
      </div>
      <div>
        <a href="https://links.isthisblog.com">links</a>
      </div>
    </div>
  )
}

export default Bio

import React from 'react';
import kebabCase from 'lodash/kebabCase';
import {graphql} from 'gatsby';

import Bio from '../components/bio';
import Layout from '../components/layout';
import Link from '../components/link';
import SEO from '../components/seo';
import {rhythm, scale} from '../utils/typography';

function BlogPostTemplate(props) {
  const post = props.data.markdownRemark;
  const siteTitle = props.data.site.siteMetadata.title;
  const {previous, next} = props.pageContext;

  return (
    <Layout
      location={props.location}
      title={siteTitle}
      js={post.frontmatter.js}>
      <SEO
        title={post.frontmatter.title}
        description={post.frontmatter.description || post.excerpt}
        css={post.frontmatter.css}
      />
      <h1
        style={{
          marginTop: rhythm(1),
          marginBottom: 0,
        }}>
        {post.frontmatter.title}
      </h1>
      <p
        style={{
          ...scale(-1 / 5),
          display: `block`,
          marginBottom: rhythm(1),
        }}>
        {post.frontmatter.date}
      </p>
      <div
        style={{
          ...scale(-1 / 5),
          display: `flex`,
          marginBottom: rhythm(1),
        }}>
        {post.frontmatter.tags &&
          post.frontmatter.tags.map((tag, key) => (
            <Link
              style={{marginRight: '10px'}}
              key={`${tag}-${key}`}
              to={`/tags/${kebabCase(tag)}/`}>
              {tag}
            </Link>
          ))}
      </div>
      <div dangerouslySetInnerHTML={{__html: post.html}} />
      <hr
        style={{
          marginBottom: rhythm(1),
        }}
      />
      <Bio />

      <ul
        style={{
          display: `flex`,
          flexWrap: `wrap`,
          justifyContent: `space-between`,
          listStyle: `none`,
          padding: 0,
        }}>
        <li>
          {previous && (
            <Link to={previous.fields.slug} rel="prev">
              ← {previous.frontmatter.title}
            </Link>
          )}
        </li>
        <li>
          {next && (
            <Link to={next.fields.slug} rel="next">
              {next.frontmatter.title} →
            </Link>
          )}
        </li>
      </ul>
    </Layout>
  );
}

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    markdownRemark(fields: {slug: {eq: $slug}}) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
        tags
      }
    }
  }
`;
